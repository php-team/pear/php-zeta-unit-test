php-zeta-unit-test (1.2.6-1) unstable; urgency=medium

  [ Derick Rethans ]
  * Fix dynamic property generation warning
  * Go with 1.2.6

  [ David Prévot ]
  * Update Standards-Version to 4.7.0

 -- David Prévot <taffit@debian.org>  Wed, 09 Oct 2024 14:03:32 +0200

php-zeta-unit-test (1.2.5-1) unstable; urgency=medium

  [ Derick Rethans ]
  * Make 'ezcTestConstraintSimilarImage::evaluate' compatible with newer
    PHPUnit versions
  * Go with 1.2.5

  [ David Prévot ]
  * Update standards version to 4.6.2, no changes needed.
  * Use execute_after_ instead of override_
  * Don’t impose PHPUnit version (Closes: #1039852)

 -- David Prévot <taffit@debian.org>  Wed, 24 Jan 2024 18:05:36 +0100

php-zeta-unit-test (1.2.4-1) unstable; urgency=medium

  [ Derick Rethans ]
  * Go with 1.2.4

 -- David Prévot <taffit@debian.org>  Tue, 26 Jul 2022 07:48:01 +0200

php-zeta-unit-test (1.2.3-1) unstable; urgency=medium

  [ Derick Rethans ]
  * Bump PHPUnit requirement to ^9.0

  [ David Prévot ]
  * Drop patch not needed anymore
  * Update Standards-Version to 4.6.1

 -- David Prévot <taffit@debian.org>  Tue, 19 Jul 2022 08:01:38 +0200

php-zeta-unit-test (1.1.7-1) unstable; urgency=medium

  [ Derick Rethans ]
  * Go with 1.1.7

 -- David Prévot <taffit@debian.org>  Sat, 12 Feb 2022 12:23:49 -0400

php-zeta-unit-test (1.1.5-1) unstable; urgency=medium

  [ David Prévot ]
  * Fix d/watch after hosting change

  [ Benjamin Eberlei ]
  * Add compatibility for old PHPUnit 3/4 getMock() API.

  [ Derick Rethans ]
  * Make sure that the directory is a non-empty variable

 -- David Prévot <taffit@debian.org>  Sat, 22 Jan 2022 09:52:30 -0400

php-zeta-unit-test (1.1.3-1) unstable; urgency=medium

  [ Derick Rethans ]
  * PHPUnit 8.0 compatibility fixes
  * Go with 1.1.3

  [ David Prévot ]
  * Simplify gbp import-orig
  * Install dh-sequence-* instead of using dh --with
  * Bump debhelper from old 12 to 13.
  * Set upstream metadata fields:
    Bug-Database, Bug-Submit, Repository, Repository-Browse.
  * Update standards version to 4.6.0, no changes needed.
  * Set Rules-Requires-Root: no.
  * Install /u/s/p/{autoloaders,overrides} files
  * Rename main branch to debian/latest (DEP-14)

 -- David Prévot <taffit@debian.org>  Sun, 10 Oct 2021 14:25:05 -0400

php-zeta-unit-test (1.1.2-2) unstable; urgency=medium

  * Allow PHPUnit 8
  * Use secure URI in debian/watch.
  * Update standards version, no changes needed.

 -- David Prévot <taffit@debian.org>  Tue, 27 Aug 2019 23:27:34 -1000

php-zeta-unit-test (1.1.2-1) unstable; urgency=medium

  [ Remi Collet ]
  * fix ezcTestConstraintSimilarImage for recent phpunit

  [ Derick Rethans ]
  * Renamed and updated license file
  * Require PHPUnit 7.x, doing 8.x is going to bit too far right now.
  * Go with 1.1.2.

  [ David Prévot ]
  * Update for recent version of PHPUnit
  * Update copyright

  [ Jeremy Bicha ]
  * Disable Ubuntu's PNG mangling (LP: #1549046)
  * Add minimal debian/gbp.conf

 -- David Prévot <taffit@debian.org>  Fri, 01 Mar 2019 10:25:37 -1000

php-zeta-unit-test (1.0.2-4) unstable; urgency=medium

  * Move repository to salsa.d.o
  * Use https in Format
  * Drop get-orig-source target
  * Use debhelper-compat 12
  * Update Standards-Version to 4.3.0
  * Install NOTICE
  * Update for recent version of PHPUnit

 -- David Prévot <taffit@debian.org>  Thu, 14 Feb 2019 12:51:10 -1000

php-zeta-unit-test (1.0.2-3) unstable; urgency=medium

  * Rebuild with recent pkg-php-tools for the PHP 7.0 transition

 -- David Prévot <taffit@debian.org>  Wed, 23 Mar 2016 16:12:23 -0400

php-zeta-unit-test (1.0.2-2) unstable; urgency=medium

  [ David Prévot ]
  * Add homemade autoload
  * Install doc a bit less far away
  * Update Standards-Version to 3.9.7

  [ Remi Collet ]
  * fix ezcTestConstraintSimilarImage for recent phpunit

 -- David Prévot <taffit@debian.org>  Fri, 12 Feb 2016 22:49:25 -0400

php-zeta-unit-test (1.0.2-1) unstable; urgency=medium

  [ David Prévot ]
  * Fix source URL

  [ Guillaume LECERF ]
  * Fix ezcTestRegressionTest::suite() as ezcMvcRegressionSuite no longer exists

  [ Derick Rethans ]
  * Go with 1.0.2.

 -- David Prévot <taffit@debian.org>  Tue, 14 Oct 2014 14:17:17 -0400

php-zeta-unit-test (1.0.1-1) unstable; urgency=low

  * Initial release

 -- David Prévot <taffit@debian.org>  Sun, 28 Sep 2014 12:19:07 -0400
